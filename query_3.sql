-- ******* 03
-- Per ogni intervallo delimitato da una coppia di date successive (ovvero la prima data è precedente alla seconda) in `dates.csv` dove la prima data ha codice "ST" e la seconda ha codice "ED" e tra le due non appaiono date con codice "ST" o "ED", trovare tra le azioni in `symbols.csv` quelle con la minima differenza tra i suoi valori "high" nell'intervallo estremi esclusi.
-- Restituire: start, end, symbol, company_name, d_high

-- Esempio
--    start    |    end     | symbol  |        company_name         | d_high
-- ------------+------------+---------+-----------------------------+--------
--  2001-10-01 | 2001-12-01 | RMKR    | Rainmaker Systems Inc.      |  0.110
--  2001-12-01 | 2001-12-05 | FCH     | FelCor Lodging Trust        |  0.010
--  2001-12-20 | 2001-12-31 | MBLA    | National Mercantile Bancorp |  0.000

\c db2016
SET search_path TO assignment3,public;

WITH getdate AS (
  SELECT D1.date AS start_date, (SELECT date FROM dates D2 WHERE D1.date < D2.date AND D2.type='ED' LIMIT 1) AS end_date
  FROM dates D1
  WHERE D1.type='ST'
), gethigh AS (
  SELECT T.start_date, T.end_date, S1.symbol, (MAX(S1.high)::DECIMAL(20,3) - MIN(S1.high)::DECIMAL(20,3)) AS d_high
  FROM getdate T, stock_price S1, symbols S2
  WHERE S1.symbol=S2.symbol AND S1.date > T.start_date AND S1.date < T.end_date
  GROUP BY S1.symbol, T.start_date, T.end_date
), getminhigh AS (
  SELECT start_date, end_date, MIN(d_high) AS d_high
  FROM gethigh
  GROUP BY start_date, end_date
)

SELECT gethigh.start_date, gethigh.end_date, symbol, company_name,
                            gethigh.d_high AS d_high
FROM gethigh JOIN getminhigh ON gethigh.d_high = getminhigh.d_high AND
                                gethigh.start_date = getminhigh.start_date AND
                                gethigh.end_date = getminhigh.end_date
             NATURAL JOIN stock_symbols;
