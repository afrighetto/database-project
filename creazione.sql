\c db2016

CREATE SCHEMA assignment3;

SET search_path TO assignment3, public;

CREATE TABLE companies (
  name TEXT,
  address TEXT,
  city TEXT,
  state CHAR(2) NOT NULL,
  zipcode INTEGER,
  phone TEXT,
  website TEXT,
  general_email TEXT,
  ceo_name TEXT,
  ceo_email TEXT,
  PRIMARY KEY (name)
);

CREATE TABLE stock_symbols (
  market TEXT,
  company_name TEXT,
  symbol TEXT,
  PRIMARY KEY (symbol, market)
);

CREATE TABLE stock_price (
  symbol TEXT,
  date DATE,
  price DECIMAL(40, 16),
  volume DECIMAL(40, 12),
  open REAL,
  low REAL,
  high REAL,
  PRIMARY KEY (symbol, date)
);

CREATE INDEX ON stock_price (date);

CREATE TABLE market (
  market CHAR(6)
);

CREATE TABLE dates (
  date DATE,
  type CHAR(2)
);

CREATE TABLE symbols (
  symbol TEXT
);
