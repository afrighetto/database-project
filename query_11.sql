-- ******* 11
-- Per ogni mercato (NYSE, NASDAQ), per ogni anno restituire la differenza (massimo-medio)
-- tra il massimo e il medio prezzo ufficiale (`price`),
-- e il massimo e il medio numero di azioni (`volume`)
-- Restituire market, year, d_price, d_volume

-- N.b. http://www.postgresql.org/docs/9.3/static/functions-datetime.html
-- SELECT EXTRACT(year FROM date) FROM dates;

-- Esempio
--  market | year |       d_price        |        d_volume
-- --------+------+----------------------+------------------------
--  NASDAQ | 2001 | 410.5847028427922732 | 123466105.030004818292
--  NYSE   | 2001 | 497.2183607611990569 |  76338791.855843718424

\c db2016
SET search_path TO assignment3,public;

SELECT market, year, MAX(price)-AVG(price) AS d_price,
                     MAX(volume)-AVG(volume) AS d_volume
FROM (SELECT market, extract (year FROM date) AS year, price, volume
      FROM stock_symbols NATURAL JOIN stock_price
     ) AS partial_table
GROUP BY market, year;
