-- ******* 02
-- Per ogni data in `dates.csv` con codice `MN` restituire la differenza
-- `(totale - scelto)` tra il numero di azioni (`volume`) totali sommando i
-- due mercati (NYSE, NASDAQ) e il numero di azioni totali nel mercato
-- scelto (in `market.csv`).
-- Restituire date, market, d_volume

-- N.b. http://www.postgresql.org/docs/9.3/static/functions-datetime.html
-- SELECT EXTRACT(year FROM date) FROM dates;

-- Esempio
--     date    | market |  d_volume
-- ------------+--------+------------
--  2001-11-27 | NYSE   | 1573057000
--  2001-12-21 | NYSE   | 1658945900

\c db2016
SET search_path TO assignment3,public;

SELECT date, (SELECT market FROM market), SUM(volume)::INTEGER AS d_volume
FROM stock_price NATURAL JOIN stock_symbols
WHERE market != (SELECT market FROM market) AND date IN (SELECT date FROM dates WHERE type = 'MN')
GROUP BY date;
