-- [query_00] Ricostruire i file .csv
-- utilizzati in input ordinandoli su date , market e name
-- ( a seconda dei campi contenuti) e salvati nella cartella /tmp/output .
--
-- in particolare, ordinare solo i file:
-- `companies.csv` su `name`
-- `stock_symbols.csv` su `market` e `company_name`
-- `stock_price.csv` su `date` e `symbol`
--
-- Il risultato può essere verificato col comando da terminale
-- diff --brief <( sort file_originale.csv ) <(sort risultato_query.csv)
--
-- Esempio di struttura della query per `companies.csv`
-- COPY( SELECT [...] )
-- TO '/tmp/output/companies.csv`'
-- WITH [...];

\c db2016
SET search_path TO assignment3,public;

COPY (SELECT * FROM companies ORDER BY name) TO '/tmp/output/companies.csv' WITH DELIMITER ';' CSV NULL 'None';
COPY (SELECT * FROM stock_symbols ORDER BY (market, company_name)) TO '/tmp/output/stock_symbols.csv' CSV NULL 'None' FORCE QUOTE company_name;
COPY (SELECT * FROM stock_price ORDER BY (date, symbol)) TO '/tmp/output/stock_price.csv' CSV NULL 'None';
